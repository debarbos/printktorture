# Console Blast README

## Ogness' Note
From John Ogness:

Tests using the CONFIG_PREEMPT_RT and CONFIG_PREEMPT preemption models
would be very helpful. We are interested in seeing if the kernel logs
are reliably dumped on panic.

I have attached a simple script that can be used to perform the test. It
just needs to be run as root with no arguments. It will blast the
console from all the CPUs for about 15 seconds, then deliberately crash
the kernel.

If all goes well, you should see the full backtrace of the crash,
including the final banner:

---[ end Kernel panic - not syncing: sysrq triggered crash ]---

If you see this banner, the test was successful.

Let me know if you have any problems with the test or any printk
problems in general.

## Success Conditions

- Determine if "Kernel Panic - not syncing: sysrq triggered crash:" is printed to the
  console.

## Prerequisites
- 8250 UART used as a console.
- Console set to  console=ttyS0,115200
- (Optional) crashkernel configured.
- A CONFIG_PREEMPT_RT or CONFIG_PREEMPT kernel.
