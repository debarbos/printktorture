# printktorture

## Purpose

To test printk upstream in preparation for the (soon TM) merge.

## Contents

console_blast -- John Ogness' printk test script, see README for more details

printk_kthread -- an adapted script from NEC that employs an artificial load to
stress test printk/NMI/crashkernel interactions.

max_contention -- a combination of console_blast and printk_kthread.

qemu_kvm -- a quick shell command made to test the corner case where printk may
hang due to early panics.

pty_lockup -- a test to discover any softlockups created by printk

stress_ng -- a test to highlight any bugs that may be caused by migrating
threads across NUMA nodes
