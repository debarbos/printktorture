# Kthreads & Printk stress test  README 

## Overview

This test will act as a "stress test" for the console via the loading of a custom kernel module that spawns kernel threads. 
These threads continuously call the `printk` function to generate log messages. 

Additionally, the test includes a step to interact with the system's 
Intelligent Platform Management Interface (IPMI) using `ipmitool` to simulate the invocation of NMIs 
on said systems.

The "flavors" of this test include: 
- A basic installation/removal of the KMOD.
- A normal NMI invocation
- NMI + timed sysrq crashes
- A scenario in which NMIs will cause a kernel panic via sysctl tunables.

The success condition for each of these scenarios is described within the tests
themselves.

Please note the authorship/copyrighted code. I am not responsible for any damage
or behavior you may encounter on your system.

## Success Conditions

- Basic: "Finishing Worker Test" printed to the console. No
  crashes/panic/stalls.
- NMI: Same as Basic. Note any RCU CPU stall warnings. 
- Crash: Check that "Kernel panic - not syncing: sysrq triggered crash" is
  printed.
- Panic: No stalls on panic. System is able to reboot and generate a vmcore with
  appropriate logs.

## Prerequisites

- A Linux system with kernel module support.
- The `ipmitool` utility installed on your system.
- 8250 UART used as a console.
- Console set to  console=ttyS0,115200
- (Optional) crashkernel configured.
- Have the attached (simple) kernel module built and ready to be loaded.
