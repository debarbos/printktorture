#!/bin/bash

# Description:
#
# This testcase, the "crash" testcase, is a "sanity check" for any of the odd
# behavior that can occur when flushing a fully saturated console due to a
# system crash.
# 
# Check that  ---[ Kernel panic - not syncing: sysrq triggered crash ]--- is
# printed.

# Set the tunable to reboot after a panic. You may have to manually reboot your
# system.
echo 1 > /proc/sys/kernel/panic

# Load the KMOD
insmod ./kmod/kthread-noise.ko

# Wait for the console to fully saturate.
sleep 10 

echo c > /proc/sysrq-trigger
