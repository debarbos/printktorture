#!/bin/bash

# Description:
#
# Load and Unload the stress-test KMOD. This shouldn't be any trouble.
# If monitoring the system's console, take note of a "Finishing worker test"
# message.

# Load the KMOD
insmod ./kmod/kthread-noise.ko

# Allow KMOD to fully saturate the console.
sleep 15 

# Remove the KMOD
rmmod kthread-noise
