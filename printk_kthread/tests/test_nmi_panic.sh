#!/bin/bash

# Description:
#
# This testcase, the "panic" testcase, is a "sanity check" for any of the odd
# behavior that can occur when flushing a fully saturated console due to an
# NMI-induced panic.
# 
# Older implementations of printk (mainly, prior to the Dublin LPC rewrite)
# would reach a deadlock where two different CPUs contest
# the console lock.
#
# This behavior would often cause the system to temporarily lockup, and prevent
# vmcores from being generated.

for TUNABLE in panic_on_io_nmi panic_on_unrecovered_nmi unknown_nmi_panic panic;
do
    echo 1 > /proc/sys/kernel/$TUNABLE
done

insmod ./kmod/kthread-noise.ko

sleep 30

ipmitool power diag
