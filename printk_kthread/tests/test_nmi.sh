#!/bin/bash

# Description:
#
# Unlke the "panic" testcase, this testcase, the "nmi" testcase, does not
# include changing the sysctl tunables to panic on NMI. 
# 
# This should not cause a crash. It may, however, cause the kernel to print an
# RCU CPU Stall Warning. This can occur when the console can't keep up with
# message rates/flushing the console. Take note of this on CONFIG_PREEMPT
# kernels. 

# Load the KMOD
insmod ./kmod/kthread-noise.ko

# Ensure the console is fully saturated
sleep 15

# Simulate an NMI on an IPMI-supported machine.
ipmitool power diag 

# Remove the KMOD
rmmod kthread-noise
