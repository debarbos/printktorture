#!/bin/bash

# The purpose of this test is to create a "handwavey" demonstration of
# the performance of mainline printk v.s. rt-devel printk on x86 8250 UART
#
# Effectively, this test loads the kthread-noise kmod for about 30 seconds,
# which is the estimated time where the console log buffers will be fully
# saturated. It is not uncommon to see dropped messages here.

echo "--- BEGIN TEST ---"

sleep 1
echo "Inserted at $(date +%m:%d:%T)" | tee -a /dev/ttyS0 && insmod ./kmod/kthread-noise.ko


sleep 30

echo "Removed at $(date +%m:%d:%T)" | tee - /dev/ttyS0 && rmmod kthread-noise


echo "please wait for messages to finish flushing..."
if dmesg | grep -iq "Finishing worker test"; then
        echo "--- END TEST ---"
fi

