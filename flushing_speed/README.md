The purpose of this test is to measure the flushing speed of a given console. 

This test also uses the kthread-noise module. Here, we simply insert the kernel
module, wait 30 seconds, then remove the kernel module.

There is no clear success condition for this test case, we are simply measuring
the average amount of time it takes to print any pending messages after `rmmod`
is invoked.

We keep track of when the module was inserted, note the elapsed time at module
removal, and note the elapsed time when pending messages are printed, and the
worker-completion confirmation message is printed.

In summary, invoking rmmod while the system is bogged and the consoles are
hammered doesn't immediately remove the module, so we are measuring how long *on
average* it takes to receive "final confirmation" from the serial console after
inserting the module, and waiting 30 seconds.

