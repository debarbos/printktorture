#!/bin/bash
#
# Description:
#
# This testcase in the max_contention suite, combines both
# aspects of the printk_kthread test, and the console_blast tests. 
#
# By "blasting" the console with pinned processes triggered by the shell, 
# and spamming the console via printk from a loaded kernel module, 
# we can examine the system's behavior under a much more "intense" 
# load on the console with multiple processes/threads competing for a 
# single resource.
#

set -e

for TUNABLE in panic_on_io_nmi panic_on_unrecovered_nmi unknown_nmi_panic panic;
do
  echo 1 > /proc/sys/kernel/$TUNABLE
done


if [ $# -eq 0 ]; then
    first_cpu_entry=true

	cpus=$(($(nproc) - 1))
	for i in $(seq 0 $cpus); do
        if $first_cpu_entry; then
            insmod ./kmod/kthread-noise.ko
            first_cpu_entry=false
        fi
		$0 $i &
	done

	sleep 15
	echo c > /proc/sysrq-trigger
	exit 0
fi

# Setup the CPU mask for the provided CPU number. The real taskset(1)
# could simply use -c, but the busybox(1) variant does not support this.
mask=$(printf "0x%x\n" $((1 << $1)))
taskset -p $mask $$

# Wait a moment before beginning blast.
sleep 2

while true; do
	echo t > /proc/sysrq-trigger
done

