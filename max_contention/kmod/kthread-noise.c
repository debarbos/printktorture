/*
 * kthread-example.c:
 * Copyright (C) 2012 NEC Corporation
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>

static unsigned int nr_workers = 4;
static struct task_struct **workers;

static int worker(void *unused)
{
	while (!kthread_should_stop()) {
		printk("uhhh...\n");
		schedule_timeout_uninterruptible(1);
	}

	return 0;
}

static int create_workers(int nr)
{
	int i;

	if (!nr)
		return -EINVAL;

	printk("Creating %d worker threads\n", nr);

	workers = kzalloc(sizeof(struct task_struct *) * nr, GFP_KERNEL);
	if (!workers) {
		printk("Failed to allocate memory\n");
		return -ENOMEM;
	}

	for (i = 0; i < nr; i++) {
		workers[i] = kthread_run(worker, NULL, "example-%d", i);
		if (IS_ERR(workers[i])) {
			printk("Failed to create kthread\n");
			workers[i--] = NULL;
			for (; i >= 0; i--) {
				kthread_stop(workers[i]);
				workers[i] = NULL;
			}
			kfree(workers);
			return -ENOMEM;
		}
	}

	return 0;
}

static int __init kworker_module_init(void)
{
	return create_workers(nr_workers);
}

static void __exit kworker_module_exit(void)
{
	int i;

	printk("Finishing worker test\n");
	for (i = 0; i < nr_workers; i++) {
		if (workers[i])
			kthread_stop(workers[i]);
	}
	kfree(workers);
}

module_init(kworker_module_init);
module_exit(kworker_module_exit);

MODULE_DESCRIPTION("kthread worker example");
MODULE_AUTHOR("NEC Corporation");
MODULE_LICENSE("GPL");
