# Console Contention README

## Disclaimer
It is recommended to read the READMEs for Kthreads & John Ogness'
"Console Blast".

## Overview

This test combines the artificial load produced by the kthread workers in
"printk_kthread" kernel module (calling printk) and the generated console
contention from "Console Blast". 

The benefit of this test, as opposed to running these two tests individually is
that we can examine the behavior of the kernel when two highly synthetic loads
(one from a loaded module, one from sysrqs) contest the console.

Furthermore, we can detect whether potential data paths are affected due to the
sheer intensity of the load produced on the system, and the contention of the
console locks. 

In sum, while this test does not reflect *any* sort of realistic, enterprise,
"real-world" scenario, it can be our effective "canary in the coal mine" for any
race conditions or "slowdowns" caused by forthcoming
printk/uart/serial/tty/console/kitchen_sink changes.

Please note the authorship and copyrighted code. I am not responsible for any
damage or behavior you may encounter on your system.

## Success Condition
No stalls on panic. System is able to reboot and generate a vmcore with
appropriate logs.

## Prerequisites
See the prerequisites for [kthreads](../printk_kthread/README.md) && [console blast](../console_blast/README.md)

