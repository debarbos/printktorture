#!/bin/bash


pipe=/tmp/testpipe

function cleanup() {
    killall -q console 
    rm -f $pipe 
    echo "Exiting monitor harness"
}

trap cleanup EXIT 

if [[ ! -p $pipe ]]; then
    echo "Creating a named pipe..."
    mkfifo $pipe
else
    echo "Deleting old FIFO, making new one..."
    rm -f /tmp/testpipe
    mkfifo $pipe
fi

echo "Connecting conserver..."
unbuffer console -M conserver-02.eng.rdu2.redhat.com rt-qe-06.lab.eng.rdu2.redhat.com  > $pipe &
PID=$!

echo "Conserver connected..."

if grep --line-buffered -iq "Inserted" -F $pipe ; then
    echo
    echo "Matched insertion of kthread-noise module"
    SECONDS=0
fi

if grep --line-buffered -iq "Removed" -F $pipe ; then
    echo
    echo "Module removal detected at $SECONDS"
    REMOVED=$SECONDS
fi

if  grep --line-buffered -iq "Finishing" -F $pipe ; then
    duration=($SECONDS)
    pending_time=$((SECONDS - REMOVED))

    echo
    echo "Matched flushing and module removal at $duration"
    echo "Time to printk pending messages $pending_time"
    echo
fi

if ps -p $PID > /dev/null; then
    kill $PID
fi

