# QEMU-KVM Test README


## Overview

This test aims to ensure that early kernel panics, or panics during boot will
not cause printk to hang on a call trace.  

## Success Condition
 - VM Reboots, causing qemu to exit due to -no-reboot option.
## Prerequisites

- qemu-kvm installed and configured to system defaults.
- 8250 UART used as a console.
- Console set to  console=ttyS0,115200.
