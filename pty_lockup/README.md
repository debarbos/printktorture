# Intro

This tests assumes you have [LTP](https://github.com/linux-test-project/ltp)
cloned in this directory.

To run, invoke the runtest script.

The success condition is to not panic, as silly as that may seems.
